import SCons.Action
import SCons.Builder
import SCons.Defaults
import SCons.Node.FS
import SCons.Util
import subprocess

def sphinx_builder(target, source, env):
    cmd = "%s %s %s %s" % (env['SPHINXBUILD'], " ".join(env.get("SPHINXBUILDFLAGS", [])), source[0], target[0].dir.path)
    print("sphinx_build command: %s" % cmd)
    code = subprocess.call(cmd, shell=True)
    if code != 0:
        raise SCons.Errors.StopError("sphinx-build failed")

def sphinx_build_tool(env):
    sphinx_buildAction = SCons.Action.Action(sphinx_builder, "$SPHINXBUILDCOMSTR")
    sphinx_buildBuilder = SCons.Builder.Builder(
        action=sphinx_buildAction,
        source_factory=SCons.Node.FS.Entry,
        source_scanner=SCons.Defaults.DirScanner,
        multi=1
    )
    env.Append(BUILDERS=dict(sphinx_build=sphinx_buildBuilder))
    env["SPHINXBUILD"] = "sphinx-build"
