from __future__ import print_function
import platform
import os
import re
import fnmatch
import subprocess
import multiprocessing
from SCons.Script import *
from SCons.Script.SConscript import SConsEnvironment

SConsEnvironment.Chmod = SCons.Action.ActionFactory(
    os.chmod,
    lambda dest, mode: 'Chmod("%s", 0%o)' % (dest, mode)
)

def InstallPerm(env, dest, files, perm):
    obj = env.Install(dest, files)
    # todo: does not work with install sandbox!
    #for i in obj:
    #    env.AddPostAction(i, env.Chmod(str(i), perm))
    return dest
SConsEnvironment.InstallPerm = InstallPerm
SConsEnvironment.InstallProgram = lambda env, dest, files: InstallPerm(env, dest, files, 0o755)
SConsEnvironment.InstallHeader  = lambda env, dest, files: InstallPerm(env, dest, files, 0o644)
SConsEnvironment.InstallData    = lambda env, dest, files: InstallPerm(env, dest, files, 0o644)

genv = None # needs to be set via update_env

AddOption('--from-env', dest="from_env", action="store_true", default=False, help="use toolchain from environment (CXX, CC, CPP, AR...)")
AddOption('--libpaths-into-ld-library-path', dest="libpaths_into_ld_library_path", action="store_true", default=False, help="when using --from-env, put all LIBPATHs into LD_LIBRARY_PATH")
AddOption('--build', default='build', metavar="DIR", help="use different build dir")
AddOption('--prefix', default='/usr', type="string", nargs=1, metavar="DIR", help="installation prefix")

try:
    umask = os.umask(0o022)
except OSError: # ignore on systems that don't support umask
    pass


def update_from_env(with_libs=False):
    global genv
    
    if not GetOption('from_env'):
        return
    had_from_env_lead = [ False ]
    def output(var, value):
        if not had_from_env_lead[0]:
            had_from_env_lead[0] = True
            print("from env:")
        if isinstance(value, list):
            value_repr = repr(value)
            if len(value_repr) <= 80:
                print("  %s += %s" % (var, value_repr))
                return
            print("  %s += [" % (var))
            for item in value:
                print("    %s" % item)
            print("  ]")
            return
        if isinstance(value, dict):
            value_repr = repr(value)
            if len(value_repr) <= 80:
                print("  %s += %s" % (var, value_repr))
                return
            print("  %s += {" % (var))
            for key, item in value.items():
                print("    %s=%r," % (key, item))
            print("  }")
            return
        print("  %s = %r" % (var, value))

    seen = set()
    vars = "AR,AS,RANLIB,LD,NM,CC,CXX,CFLAGS,CXXFLAGS,CPPFLAGS,LDFLAGS,SPHINXBUILDFLAGS,SPHINXBUILD,DESTDIR".split(",")
    if with_libs:
        vars.append("LIBS")
    for var in vars:
        value = os.getenv(var)
        if value is None:
            continue
        seen.add(var)
        if var == "LDFLAGS":
            parsed = genv.ParseFlags(value)
            to_append = dict(
                LIBPATH=parsed["LIBPATH"],
                LINKFLAGS=parsed["LINKFLAGS"] + parsed["CCFLAGS"],
            )
            genv.Append(**to_append)
            output(var, to_append)

        elif "FLAGS" in var:
            parsed = value.split()
            genv.Append(**{var: parsed})
            output(var, parsed)
        elif var == "LIBS":
            to_add = []
            for l in re.split("[ ]+", value.strip()):
                if l.startswith("-l"):
                    l = l[2:]
                to_add.append(l)
            genv.Append(**{ var: to_add })
            output(var, to_add)
        else:
            genv[var] = value
            output(var, value)
    if GetOption("libpaths_into_ld_library_path") and genv["LIBPATH"]:
        ld_library_pathes = genv["LIBPATH"]
        ld_library_pathes_env = os.environ.get("LD_LIBRARY_PATH")
        if ld_library_pathes_env:
            ld_library_pathes.extend(ld_library_pathes_env.split(os.sep))
        os.environ["LD_LIBRARY_PATH"] = os.sep.join(ld_library_pathes)
    if os.getenv("SHOW_ENV"):
        print("other environment variables:")
        for key, value in os.environ.items():
            if key in seen:
                continue
            print("  %s = %r" % (key, value))
    print("")

def write_version_h(env, fn, define, sources=None):
    os_fn = str(File(fn).srcnode())    
    if not os.path.isfile(os_fn):
        print("version_h needs to be updated via git describe in build/ dir! (os_fn: %r, cwd: %r)" % (os_fn, os.getcwd()))
        cmd = env.Command(fn, [], 'echo "#define %s \\""$$(git describe)"\\"" > $TARGET' % define)
        if sources:
            [ env.Depends(sfn, cmd) for sfn in sources ]
        cmd = env.AlwaysBuild(cmd) # unconditionally execute command        
        return cmd
    print("version_h does ALREADY exist on source tree! %r" % os.getcwd())

def ln_generate(env, target, message_definitions, message_definition_dirs=[], sources=None):
    """
    target - A list of Node objects representing the target or targets to be built by this builder function. The file names of these target(s) may be extracted using the Python str function.
    source - A list of Node objects representing the sources to be used by this builder function to build the targets. The file names of these source(s) may be extracted using the Python str function.
    genv - The construction environment used for building the target(s). The builder function may use any of the environment's construction variables in any way to affect how it builds the targets
    """
    if isinstance(message_definitions, str):
        msg_defs = re.sub("[\n ,]+", " ", message_definitions)
    else:
        msg_defs = " ".join(message_definitions)
    deps = []
    
    if isinstance(message_definition_dirs, str):
        message_definition_dirs = [ message_definition_dirs ]

    message_definition_dirs.insert(0, Dir("#share/message_definitions"))
        
    md_dir_args = []
    for dn in message_definition_dirs:
        if not isinstance(dn, SCons.Node.FS.Dir):
            dn = Dir(dn)
        md_dir_args.append("-md_dir '%s'" % dn.srcnode().path)
    md_dir_arg = " " .join(md_dir_args)
        
    cmd = env.Command(target, deps, "$LN_GENERATE -o $TARGET %s%s" % (md_dir_arg, msg_defs))
    if sources:
        [ env.Depends(fn, cmd) for fn in sources ]
    cmd = env.AlwaysBuild(cmd) # unconditionally execute command
    return cmd


        
def write_config_h(target, source=None, sources=None):
    if source is None:
        source = target + ".in"
        
    def write_config_h_action(target, source, env):
        config_h_defines = env.Dictionary()
        for _target, _source in zip(target, source):
            with open(str(_source), "r") as fp:
                generated = fp.read() % config_h_defines
            with open(str(_target), "w") as fp:
                fp.write(generated)
    
    cmd = genv.AlwaysBuild(genv.Command(target, source, write_config_h_action))
    if sources:
        [ Depends(fn, cmd) for fn in sources ]    
    return cmd

def pass_environment(names):
    if isinstance(names, str):
        names = names.split(" ")
    for var in names:
        value = os.getenv(var)
        if value is not None:
            genv[var] = value

def link_to_libln(env, libs=[]):
    common_flags = [ ]
    if "__QNX__" not in env.get("CPPDEFINES", ""):
        common_flags.append("-pthread")
    
    build_dir = GetOption('build')
    env.Prepend(
        CPPPATH=[ "#libln/include", "./src" ],
        LIBPATH=[ os.path.join("#" + build_dir, "libln") ],
    )
    if not hasattr(env, "need_to_link_rt") or env.need_to_link_rt:
        libs = ["rt"] + libs
    env.Append(
        LIBS=[ "ln" ] + libs,
        CFLAGS=common_flags,
        CXXFLAGS=common_flags,
        LINKFLAGS=common_flags,
    )

def flatten_subdirs(subdirs_in, default=None, first=["libln"]):
    subdirs = {} # user-alias -> base-path
    def walk_subdirs(subdirs_in, base=""):
        this_dirs = {}
        for dn in subdirs_in:
            if isinstance(dn, str):
                this_dirs[dn] = subdirs[dn] = os.path.join(base, dn)
                continue
            # assume list
            this_dirs[dn[0]] = subdirs[dn[0]] = walk_subdirs(dn[1], os.path.join(base, dn[0]))
        return this_dirs
    walk_subdirs(subdirs_in)
    if default is None:
        default = []
        for dn in subdirs_in:
            if isinstance(dn, str):
                default.append(dn)                
                continue
            default.append(dn[0])
    if not isinstance(default, str):
        default = ",".join(default)
    
    AddOption('--subdirs', dest="subdirs", default=default, help="build these subdirs (default: %r)" % default)
    user_subdirs = re.split("[, ]+", GetOption("subdirs"))
    all_subdirs = []
    def add_subdirs(user_subdirs):
        for subdir in user_subdirs:
            base_path = subdirs[subdir]
            if isinstance(base_path, dict):                
                add_subdirs(base_path.keys())
                continue
            if base_path in all_subdirs:
                continue
            if base_path in first:
                all_subdirs.insert(0, base_path)
                continue
            all_subdirs.append(base_path)
    add_subdirs(user_subdirs)
    return all_subdirs
    
# Add a custom Check function to test for structure members.
def CheckMember(context, include, decl, member, include_quotes="<>"):
    context.Message("Checking for member %s in %s..." % (member, decl))
    text = """
#include %(header)s
int main(){
  %(decl)s test;
  (void)test.%(member)s;
  return 0;
};
""" % { "header" : include_quotes[0] + include + include_quotes[1],
        "decl" : decl,
        "member" : member,
        }

    ret = context.TryCompile(text, extension=".cc")
    context.Result(ret)
    return ret

def popen(cmd, verbose=False, decode="utf-8", with_returncode=False):
    """
    for now ignore return value or stderr output
    """
    #if isinstance(cmd, list): cmd.insert(0, cmd[0])
    if verbose: print("popen execute %r" % (cmd, ))
    p = subprocess.Popen(cmd, shell=not isinstance(cmd, list), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if verbose:
        if out: print("stdout:\n%s" % out)
        if err: print("stderr:\n%s" % err)
        print("return %r" % p.returncode)
    if decode is not None:
        out = out.decode(decode)
        if verbose:
            print("decoded stdout:\n%s" % out)
    if with_returncode:
        return out, err, p.returncode
    return out

def env_popen(env, cmd, verbose=False, decode="utf-8", with_returncode=False):
    if isinstance(cmd, tuple):
        cmd = list(cmd)
    if isinstance(cmd, list):
        for i, arg in enumerate(cmd):
            cmd[i] = env.subst(arg)
    else:
        cmd = env.subst(cmd)
    return popen(cmd, verbose=env.get("VERBOSE") or verbose, decode=decode, with_returncode=with_returncode)

def install_mds(env, base_dir, excludes=[], target=None):
    root = base_dir.srcnode().abspath
    project_root = Dir("#").srcnode().abspath
    #print("project_root: %r" % project_root)
    #print("root: %r" % root)
    if target is None:
        rel_to_project_root = root[len(project_root) + 1:].split("/", 1)[0] # todo: win32: split here by os.SEP?
        #print "rel_to_project_root", rel_to_project_root
        target = rel_to_project_root
        
    all_mds = {}
    dir_type = type(Dir("."))
    def collect_in(base):
        for md in Glob(base + "/*"):
            if isinstance(md, dir_type):
                #print("skip dir %r for now" % md)
                collect_in(str(md))
                continue
            this_base = base[len(root):]
            if this_base not in all_mds:
                all_mds[this_base] = []
            rfn = str(md)[len(root) + 1:]
            #print("md: %s, rfn: %s" % (md, rfn))
            for exc in excludes:
                if fnmatch.fnmatch(rfn, exc):
                    #print("excluded by pattern %r: %r" % (exc, rfn))
                    break
            else:
                all_mds[this_base].append(md)
    collect_in(root)
    for base, mds in all_mds.items():
        env.InstallData("$DATADIR/%s/message_definitions/%s" % (target, base), mds)
    
def find_define_value(fn, define, default=None):
    contents = open(File(fn).srcnode().abspath).read()
    contents = re.sub(r"(?ms)/\*.*?\*/", "", contents)
    for line in contents.split("\n"):
        line = line.split("//", 1)[0].strip()
        m = re.match("#.*define[ \t]%s[ \t]+([^ \t]+)" % define, line)
        if m is not None:
            return m.group(1)
    return default

def update_env(this_env): # rename this to TOOL_LN
    # compare https://bitbucket.org/scons/scons/wiki/InstallFiles
    global genv
    genv = this_env
    VERBOSE_VALUE = ARGUMENTS.get("VERBOSE", ARGUMENTS.get("V"))
    genv["VERBOSE"] = VERBOSE_VALUE is not None and VERBOSE_VALUE[:1] != "0"

    print("python version used for scons: %s" % platform.python_version())
    
    update_from_env()
    
    # todo: use env.AddMethod() instead!
    genv.AddMethod(install_files, "install_files")
    genv.AddMethod(replace_shebang, "replace_shebang")
    genv.AddMethod(env_popen, "popen")
    genv.AddMethod(install_mds, "install_mds")
    genv.AddMethod(ln_generate, "ln_generate")
    genv.AddMethod(write_version_h, "write_version_h")
    genv.write_config_h = write_config_h
    genv.pass_environment = pass_environment
    genv.find_define_value = find_define_value
    genv.Append(ENV=dict(
        LN_DEBUG=os.getenv("LN_DEBUG", "")        
    ))
    genv["PREFIX"] = prefix = GetOption("prefix")
    
    genv.Decider('MD5-timestamp')

    if not genv["VERBOSE"]:
        genv["CCCOMSTR"]     = "cc      $SOURCE",
        genv["CXXCOMSTR"]    = "c++     $SOURCE",
        genv["LINKCOMSTR"]   = "link    $TARGET"
        genv["SHCCCOMSTR"]   = "sh.cc   $SOURCE",
        genv["SHCXXCOMSTR"]  = "sh.c++  $SOURCE",
        genv["SHLINKCOMSTR"] = "sh.link $TARGET"

    n_jobs = GetOption('num_jobs')
    if n_jobs == 1: # not specified or at default value
        max_jobs = multiprocessing.cpu_count() + 2
        if not genv["VERBOSE"] or VERBOSE_VALUE.lower() == "parallel":
            n_jobs = max_jobs
        else:
            n_jobs = 1
        SetOption('num_jobs', n_jobs)

    revision = genv.popen("git rev-parse HEAD").strip()
    print("git revision of HEAD: %s" % revision)

def is_exe(fpath):
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

def is_matching(name, patterns):
    for pattern in patterns:
        if fnmatch.fnmatch(name, pattern):
            return True
    return False

def get_files(base, excludes=[]):
    chdir = str(Dir(".").srcnode())
    fns = {}
    def list_files(base):
        for e in os.listdir(os.path.join(chdir, base)):
            if e[0] == ".":
                continue
            if is_matching(e, excludes):
                continue
            p = os.path.join(base, e)
            if is_matching(p, excludes):
                continue
            rp = os.path.join(chdir, p)
            if os.path.isdir(rp):
                list_files(p)
                continue
            if base not in fns:
                fns[base] = ([], []) # execs, data
            if is_exe(rp):
                fns[base][0].append(p)
            else:
                fns[base][1].append(p)
    list_files(base)
    return fns

def install_files(env, target, base, excludes=[]):
    if not target.endswith("/"):
        target += "/"
    for base, (execs, data) in get_files(base, excludes).items():
        if execs: env.InstallProgram(target + base, execs)
        if data:  env.InstallData(target + base, data)

def replace_shebang(env, target, src, shebang, search_abs_path=False):
    if search_abs_path and not os.path.isabs(shebang):
        # try to find absolute path!
        if sys.version_info[:2] >= (3,3):
            import shutil
            shebang_ = shutil.which(shebang)
        else:
            for path in os.environ.get("PATH", "/usr/bin:/bin").split(os.pathsep):
                shebang_ = os.path.join(path, shebang)
                if os.path.isfile(shebang_) and os.access(shebang_, os.X_OK):
                    break
            else:
                shebang_ = None
        if shebang_:
            shebang = shebang_

    def write_with_shebang(target, source, env):
        for _target, _source in zip(target, source):
            full_src = str(_source)
            #print("source: %r" % full_src)
            with open(full_src, "r") as fp:
                data = fp.read()
            org_mode = os.stat(full_src).st_mode
            #print("org_mode: %o" % org_mode)

            if data.startswith("#!"):
                old_shebang, data = data.split("\n", 1)
            new_data = "#!%s\n%s" % (shebang, data) # todo: eval

            full_dst = str(_target)
            #print("full_dst: %r" % full_dst)
            with open(full_dst, "w") as fp:
                fp.write(new_data)
            os.chmod(full_dst, org_mode)
    cmd = env.Command(target, src, write_with_shebang)
    return cmd
